//
//  _0220712_RabinShahi_NYCSchoolsApp.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import SwiftUI

@main
struct _0220712_RabinShahi_NYCSchoolsApp: App {
    
    let dataService = SchoolDataService(url: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!)
    
    var body: some Scene {
        WindowGroup {
            SchoolListView(dataService: dataService)
        }
    }
}
