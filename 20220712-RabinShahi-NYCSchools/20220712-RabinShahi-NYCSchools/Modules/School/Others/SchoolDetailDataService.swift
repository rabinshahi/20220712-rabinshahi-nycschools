//
//  SchoolDetailDataService.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation
import Combine

/*
 Protocol for school detail service
 */
protocol SchoolDetailDataServiceProtocol {
    func getSchoolDetail() -> AnyPublisher<[SchoolDetailModel], Error>
}

/*
 Network layer for school detail
 */
class SchoolDetailDataService: SchoolDetailDataServiceProtocol{
    
    // MARK: Properties
    let url:URL
    
    // MARK: initializer
    init(url:URL){
        self.url = url
    }
    
    // MARK: Method
    func getSchoolDetail() -> AnyPublisher<[SchoolDetailModel], Error> {
        URLSession.shared.dataTaskPublisher(for: url)
            .map({$0.data})
            .decode(type: [SchoolDetailModel].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
