//
//  SchoolDataService.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation
import Combine

/*
 Protocol for school list service
 */
protocol SchoolDataServiceProtocol {
    func getSchools() -> AnyPublisher<[SchoolModel], Error>
}

/*
 Network layer for school list
 */
class SchoolDataService: SchoolDataServiceProtocol{
    
    // MARK:  Properties
    let url:URL
    
    // MARK: Initializer
    init(url:URL){
        self.url = url
    }
    
    // MARK: Methods
    func getSchools() -> AnyPublisher<[SchoolModel], Error> {
        URLSession.shared.dataTaskPublisher(for: url)
            .map({
                $0.data
            })
            .decode(type: [SchoolModel].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
