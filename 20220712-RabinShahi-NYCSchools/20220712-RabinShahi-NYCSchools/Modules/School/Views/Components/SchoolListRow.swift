//
//  SchoolListRow.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 13/07/2022.
//

import SwiftUI

/*
 create school list row
 */
struct SchoolListRow: View {
    
    // MARK: Properties
    let title:String
    
    // MARK: Initializer
    init(title:String) {
        self.title = title
    }
    
    // MARK: View
    var body: some View {
        VStack(alignment:.leading) {
            Text(self.title)
            Text("Show Details")
                .padding(.top, 4)
                .font(Font.body.bold())
        }.padding()
    }
}

struct SchoolListRow_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListRow(title: "Value")
    }
}
