//
//  SchoolDetailRow.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 13/07/2022.
//

import SwiftUI

/*
 create school detail item row
 */
struct SchoolDetailRow: View {
    
    // MARK: Properties
    let title:String
    let subTitle:String
    
    // MARK: Initializer
    init(title:String, subtitle:String) {
        self.title = title
        self.subTitle = subtitle
    }
    
    // MARK: View
    var body: some View {
        HStack() {
            Text(title)
            Spacer()
            Text(subTitle)
                .font(Font.body.bold())
        }.padding(.bottom, 6)
    }
}

struct SchoolDetailRow_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailRow(title: "Title", subtitle: "Subtitle")
    }
}
