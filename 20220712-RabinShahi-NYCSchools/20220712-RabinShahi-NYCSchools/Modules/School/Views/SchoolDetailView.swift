//
//  SchoolDetailView.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import SwiftUI

/*
 Create School detail view
 */
struct SchoolDetailView: View {
    
    // MARK: - Properties
    let school:SchoolModel
    @State var isFist = true
    
    @StateObject private var vm: SchoolDetailViewModel
    
    // MARK: - Initializer
    init(school:SchoolModel) {
        print(school.dbn)
        self.school = school
        let aUrl = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(school.dbn)")!
        _vm = StateObject(wrappedValue: SchoolDetailViewModel(dataService: SchoolDetailDataService(url: aUrl)))
    }
    
    // MARK: - Views
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(alignment: .leading) {
                    Text(school.name)
                        .frame(alignment: .leading)
                        .font(Font.body.bold())
                        .padding(.bottom, 10)
                    
                    SchoolDetailRow(title: "Number of SAT: ",
                                    subtitle: vm.schoolDetail?.numSatTest ?? "")
                    SchoolDetailRow(title: "Reading Avg Score: ",
                                    subtitle: vm.schoolDetail?.numSatCriticalReading ?? "")
                    SchoolDetailRow(title: "Math Avg Score: ",
                                    subtitle: vm.schoolDetail?.numSatMathAvg ?? "")
                    SchoolDetailRow(title: "Writing Avg Score: ",
                                    subtitle: vm.schoolDetail?.numSatWriting ?? "")
                }
                .padding()
            }
        }
    }
}


