//
//  SchoolListView.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import SwiftUI

/*
 Create School List View
 */
struct SchoolListView: View {
    
    // MARK: - Properties
    @StateObject private var vm: SchoolListViewModel
    
    // MARK: - Initializer
    init(dataService: SchoolDataServiceProtocol) {
        _vm = StateObject(wrappedValue: SchoolListViewModel(dataService: dataService))
    }
    
    // MARK: - View
    var body: some View {
        NavigationView {
            List(vm.schools, id:\.id){ school in
                NavigationLink(destination: SchoolDetailView(school: school)) {
                    SchoolListRow(title: school.name)
                }.navigationTitle("Schools")
            }
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static let dataService = SchoolDataService(url: URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2")!)
    
    static var previews: some View {
        SchoolListView(dataService: dataService)
    }
}
