//
//  SchoolListViewModel.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation
import Combine

/*
 Create View Model for School List View
 - Fetch data from remote
 */
class SchoolListViewModel: ObservableObject {
    
    // MARK: Properties
    @Published var schools:[SchoolModel] = []
    let dataService:SchoolDataServiceProtocol
    var cancellables = Set<AnyCancellable>()
    
    // MARK: Initializer
    init(dataService: SchoolDataServiceProtocol) {
        self.dataService = dataService
        getSchools()
    }
    
    // MARK: Method to fetch schools
    func getSchools() {
        dataService.getSchools().sink { value in
             print(value)
        } receiveValue: { [weak self] result in
            self?.schools = result
        }.store(in: &cancellables)
    }
}
