//
//  SchoolDetailViewModel.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation
import Combine

/*
 Create View model for Detail Page
 - Fetch Data from remote
 */
class SchoolDetailViewModel: ObservableObject {
    
    // MARK: Properties
    @Published var schoolDetail:SchoolDetailModel?
    
    let dataService:SchoolDetailDataServiceProtocol
    var cancellables = Set<AnyCancellable>()
    
    // MARK: Initializer
    init(dataService: SchoolDetailDataServiceProtocol) {
        self.dataService = dataService
        getSchoolDetail()
    }
    
    // MARK: Method to fetch schools
    func getSchoolDetail() {
        dataService.getSchoolDetail().sink { value in
            print(value)
        } receiveValue: { [weak self] result in
            let data =  result.first
            self?.schoolDetail = data
        }.store(in: &cancellables)
    }
}
