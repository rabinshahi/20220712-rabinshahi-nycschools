//
//  SchoolModel.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation

/*
 Create School Model
 */
struct SchoolModel: Codable, Identifiable {
    var id = UUID()
    let name:String
    let dbn:String
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case dbn
    }
}
