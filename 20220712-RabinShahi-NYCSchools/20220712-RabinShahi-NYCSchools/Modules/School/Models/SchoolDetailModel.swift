//
//  SchoolDetailModel.swift
//  20220712-RabinShahi-NYCSchools
//
//  Created by Rowin Shahi on 12/07/2022.
//

import Foundation

/*
 Create School Detail Model
 */
struct SchoolDetailModel: Codable {
    let name:String
    let dbn:String
    let numSatTest: String
    let numSatCriticalReading: String
    let numSatMathAvg: String
    let numSatWriting: String
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case dbn
        case numSatTest = "num_of_sat_test_takers"
        case numSatCriticalReading = "sat_critical_reading_avg_score"
        case numSatMathAvg = "sat_math_avg_score"
        case numSatWriting = "sat_writing_avg_score"
    }
}
